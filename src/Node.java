
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }
   
   public static Node parsePostfix (String s) {
      s = s.trim();
      String anynodes = s.replaceAll("\\(","").replaceAll("\\)","").replaceAll("\\,","").trim();
      if (anynodes.length() < 1) throw new RuntimeException("Vigane avaldis: " + s);
      if (s.length() < 1) throw new RuntimeException("T�hi string");
      if (s.indexOf(",,") > -1) throw new RuntimeException("Mitmekordsed komad avaldises: " + s);
	  Node root = new Node(null, null, null);
		if (s.charAt(0) != '(') {
            checkName(s, s);
            root.name = s;
			return root;
		} else {
			int rootEnd = s.lastIndexOf(')');
			root.name = s.substring(rootEnd + 1, s.length());
            checkName(root.name, s);
			String innerPart = s.substring(1, rootEnd);
            List<String> children = getChildren(innerPart);
			root.firstChild = getNode(children, s);
		}
		return root;
   }

   public static Node getNode(List<String> children, String avaldis) {
        String child = children.get(0);
        Node childNode = new Node(null, null, null);
		if(child.charAt(0) == '(') {
            int nodeEnd = child.lastIndexOf(')');
            childNode.name = child.substring(nodeEnd + 1, child.length());
            checkName(childNode.name, avaldis);
            String innerPart = child.substring(1, nodeEnd);
            List<String> childrenDeep = getChildren(innerPart);
            childNode.firstChild = getNode(childrenDeep, avaldis);
			if (children.size() > 1) {
                children.remove(0);
                childNode.nextSibling = getNode(children, avaldis);
            }
            return childNode;
		} else {
            childNode.name = children.get(0);
            checkName(childNode.name, avaldis);
            if (children.size() > 1) {
                children.remove(0);
                childNode.nextSibling = getNode(children, avaldis);
            }
            return childNode;
        }
	}

    public static List<String> getChildren (String innerPart) {
        List<String> fixed = new ArrayList<String>();
        String[] parts = innerPart.split(",");
        for (int i = 0; i < parts.length; i++) {
            String elem = parts[i];
            int elemsNeeded = 1;
            while (elemsNeeded > 0) {
                int countLeft = 0;
                for (int j = 0; j < elem.length(); j++) if (elem.charAt(j) == '(') countLeft++;
                int countRight = 0;
                for (int j = 0; j < elem.length(); j++) if (elem.charAt(j) == ')') countRight++;
                elemsNeeded = countLeft-countRight;
                if (elemsNeeded > 0) {
                    for (int k = 1; k <= elemsNeeded; k++) {
                        elem = elem+","+parts[i+k];
                    }
                    i+=elemsNeeded;
                }
            }
            fixed.add(elem);
        }
        return fixed;
    }


   public String leftParentheticRepresentation() {
      return labi(this);
   }

   public static void checkName(String n, String avaldis) {
       if (n.indexOf('(') > -1 ||
            n.indexOf(')') > -1 ||
            n.indexOf(',') > -1 ||
            n.indexOf(" ") > -1 ||
            n.length() < 1) throw new RuntimeException("Vigane tipunimi: " + avaldis);
   }
   
   public static String labi (Node root) {
	   StringBuilder ret = new StringBuilder("");
	   ret.append(root.name);
	   if (root.firstChild != null) {
		   ret.append("(");
		   ret.append(labi(root.firstChild));
           ret.append(")");
           if (root.nextSibling != null) {
               ret.append(",");
               ret.append(labi(root.nextSibling));
           }
	   } else if (root.nextSibling != null) {
		   ret.append(",");
		   ret.append(labi(root.nextSibling));
	   }
	   return ret.toString();
   }

   public static void main (String[] param) {
      String s = "((C(hh),D))A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}

